package br.com.carrefour

import android.app.Application
import br.com.carrefour.di.appModule
import com.crashlytics.android.Crashlytics
import com.google.android.play.core.splitcompat.SplitCompatApplication
import io.fabric.sdk.android.Fabric
import org.koin.core.context.startKoin
import timber.log.Timber

/**
 * Base class for maintaining global application state.
 *
 * @see SplitCompatApplication
 */
class MCommerceApp : Application() {

    /**
     * Called when the application is starting, before any activity, service, or receiver objects
     * (excluding content providers) have been created.
     *
     * @see SplitCompatApplication.onCreate
     */
    override fun onCreate() {
        super.onCreate()
        initTimber()
        initFabric()
        startKoin {
            modules(appModule)
        }
        //initRandomNightMode()
    }


    /**
     * Initialize log library Timber only on debug build.
     */
    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    /**
     * Initialize crash report library Fabric on non debug build.
     */
    private fun initFabric() {
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, Crashlytics())
        }
    }

//    /**
//     * Initialize random nightMode to make developer aware of day/night themes.
//     */
//    private fun initRandomNightMode() {
//        if (BuildConfig.DEBUG) {
//            themeUtils.setNightMode(Random.nextBoolean())
//        }
//    }
}
