include(
    ":app",
    ":core",
    ":features:home",
    ":features:menu",
    ":libraries:test_utils",
    ":commons:ui",
    ":commons:views"
)

rootProject.buildFileName = "build.gradle.kts"
